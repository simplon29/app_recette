# OpenWeatherMap API avec Webpack

Un projet pour apprendre à utiliser l'API OpenWeatherMap avec des requêtes Ajax.

## Installation

1. Cloner le projet.
2. Ouvrir GIT BASH dans le dossier du projet.
3. Exécuter "npm install" pour installer les dépendances.
4. Exécuter "npm run dev" pour créer le build de développement.
5. Exécuter "npm run prod" pour créer le build de production.
6. Lancer le fichier index.html du répertoire "dist" pour lancer la web app.

## API OpenWeatherMap

Pour ce projet tu vas devoir utiliser l'API OpenWeatherMap, mais pour cela il te faut te créer un compte pour obtenir [ta propre clef d'API](https://home.openweathermap.org/users/sign_up). Pour savoir comment faire tes requêtes, consulte [le mode d'emploi de l'API](https://openweathermap.org/current).

## Ajax & API Fetch

Pour faire appel à l'API, il te faudra regarder du côté des [API Fetch avec Ajax](https://writecode.fr/tutoriel/requete-ajax-avec-api-fetch) ;-)
Pour en savoir un peu plus sur Ajax, tu peux aussi lire [cette série d'articles](https://webdesign.tutsplus.com/series/ajax-for-front-end-designers--cms-967).

## Encore de la POO :)

Pour ce projet, il t'est demandé à nouveau d'utiliser de la POO. Pour cela, un dossier components contient lui même un dossier WeatherCard dans lequel vous trouverez un composant WeatherDetails dans lequel tu instancieras un autre sous composant WeatherDescription.
 
![Diagramme WeatherCard](src/diagrammes/WeatherCard.jpg)

Grâce aux [imports et exports des modules ES6](https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Modules) utilisés à bon escient, tu vas pouvoir avoir un fichier JS par classe, c'est bien plus organisé ! Cette fonctionnalité était présente dans Node.js depuis longtemps et plusieurs bibliothèques et frameworks JavaScript ont permis l'utilisation de modules, notamment Webpack que nous utilisons dans ce projet.

## Bonus

- Bien entendu, mieux vaut faire trop de POO que pas assez, et si tu as du temps, tu peux t'amuser à créer un WeatherCardManager par exemple. ;-)

- Il est possible d'utiliser d'autres icônes pour la météo. Pour cela, tu peux utiliser les icônes SVG du répertoire "src/img" et remplacer celles d'office imposées par OpenWeatherMap.

![Correspondance des icônes](src/img/icon-mapping.png)

- Pourquoi ne pas tenter de créer un autre type de carte ? Avec un design différent, plus d'informations, laisse parler ton inspiration. ;-) Ceci est un exemple mais tu es libre de proposer ce que tu veux.

![Correspondance des icônes](src/img/exemple-card-bonus.jpg)

Tu peux utiliser [createapp.dev](https://createapp.dev/) comme boilerplate (c'est à dire pour créer la structure de départ d'un projet).